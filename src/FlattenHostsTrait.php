<?php

namespace Drupal\image_domain_filter;

use Drupal\Core\Form\FormStateInterface;

/**
 * Consolidated code for handling textarea input.
 */
trait FlattenHostsTrait {

  /**
   * Validation callback used to expand textarea into array of values.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   */
  public static function expandHosts(array $element, FormStateInterface $formState) {
    $value = $formState->getValue($element['#parents']);
    $value = explode("\n", $value);
    $value = array_values(array_filter(array_map('trim', $value)));
    $formState->setValueForElement($element, $value);
  }

  /**
   * Flatten an array of hosts into a string for the textarea input.
   *
   * @param array $hosts
   *   An array of hosts.
   *
   * @return string
   *   A hosts textarea value.
   */
  public static function flattenHosts(array $hosts) {
    return implode("\n", $hosts);
  }

}
