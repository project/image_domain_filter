<?php

namespace Drupal\image_domain_filter\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\image_domain_filter\FlattenHostsTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration for Image Domain Filter module settings.
 */
class ImageDomainFilterSettingsForm extends ConfigFormBase {

  use FlattenHostsTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ImageDomainFilterSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'image_domain_filter_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['image_domain_filter.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('image_domain_filter.settings');

    $url = Url::fromUri('https://www.drupal.org/docs/installing-drupal/trusted-host-settings#s-trusted-host-security-setting-in-drupal-8');
    $link = Link::fromTextAndUrl($this->t('docs'), $url);
    $params = ['@link' => $link->toString()];
    $form['allowed_hosts'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allowed hosts'),
      '#description' => $this->t('Allow images hosted from hosts matching these patterns. These are expected to be 1 per line, using regex patterns similar to the site\'s "trusted hosts" configuration. See the @link for trusted hosts.', $params),
      '#default_value' => !empty($config->get('allowed_hosts')) ? static::flattenHosts($config->get('allowed_hosts')) : '',
      '#element_validate' => [[$this, 'expandHosts']],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('image_domain_filter.settings')
      ->set('allowed_hosts', $form_state->getValue('allowed_hosts'))
      ->save();
    parent::submitForm($form, $form_state);

    // Clear any caches with text formats applied.
    $tags = [];
    $formats = $this->entityTypeManager->getStorage('filter_format')
      ->loadMultiple();
    foreach ($formats as $format) {
      $tags[] = 'config:filter.format.' . $format->id();
    }
    if ($tags) {
      Cache::invalidateTags($tags);
    }
  }

}
