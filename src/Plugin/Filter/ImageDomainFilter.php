<?php

namespace Drupal\image_domain_filter\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\image_domain_filter\FlattenHostsTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to restrict images to trusted sites.
 *
 * @Filter(
 *   id = "image_domain_filter",
 *   title = @Translation("Restrict images to trusted sites"),
 *   description = @Translation("Disallows usage of &lt;img&gt; tag sources that are not hosted by a trusted site by replacing them with a placeholder image."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = {
 *     "allow_local" = TRUE,
 *     "allow_global_domains" = TRUE,
 *     "allow_trusted_hosts" = TRUE,
 *     "allowed_hosts" = { }
 *   },
 *   weight = 9
 * )
 */
class ImageDomainFilter extends FilterBase implements ContainerFactoryPluginInterface {

  use FlattenHostsTrait;

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * ImageDomainFilter constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['allow_local'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow images from this site'),
      '#description' => $this->t('Allow images hosted from this site, similar to the core "Restrict images to this site" filter.'),
      '#default_value' => $this->settings['allow_local'],
    ];

    $link = Link::createFromRoute($this->t('configured'), 'image_domain_filter.admin_settings');
    $params = ['@link' => $link->toString()];
    $form['allow_global_domains'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Allow images from site's globally trusted hosts"),
      '#description' => $this->t('Allow images hosted from this domains @link as trusted for this site.', $params),
      '#default_value' => $this->settings['allow_global_domains'],
    ];

    $url = Url::fromUri('https://www.drupal.org/docs/installing-drupal/trusted-host-settings#s-trusted-host-security-setting-in-drupal-8');
    $link = Link::fromTextAndUrl($this->t('trusted hosts'), $url);
    $params = ['@link' => $link->toString()];
    $form['allow_trusted_hosts'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow images from trusted hosts'),
      '#description' => $this->t('Allow images hosted from sites matching the @link configuration.', $params),
      '#default_value' => $this->settings['allow_trusted_hosts'],
    ];

    $form['allowed_hosts'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allowed hosts'),
      '#description' => $this->t('Allow images hosted from hosts matching these patterns. These are expected to be 1 per line, using regex patterns similar to the site\'s "trusted hosts" configuration.'),
      '#default_value' => !empty($this->settings['allowed_hosts']) ? static::flattenHosts($this->settings['allowed_hosts']) : '',
      '#element_validate' => [[$this, 'expandHosts']],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $html_dom = Html::load($text);
    $images = $html_dom->getElementsByTagName('img');
    foreach ($images as $image) {
      $src = $image->getAttribute('src');

      // Domain/host checks.
      if ($this->settings['allow_global_domains'] && $this->isAllowedGlobal($src)) {
        continue;
      }
      if ($this->settings['allow_trusted_hosts'] && $this->isAllowedTrusted($src)) {
        continue;
      }
      if ($this->isAllowedHost($src)) {
        continue;
      }

      // Allow images found locally, similar to FilterHtmlImageSecure.
      if ($this->settings['allow_local'] && $this->isAllowedLocal($src)) {
        $image->setAttribute('src', $src);
        continue;
      }

      // If we've made it here, the image is not allowed. Invoke core handling
      // of insecure images.
      $this->moduleHandler->alter('filter_secure_image', $image);
    }

    $text = Html::serialize($html_dom);
    return new FilterProcessResult($text);
  }

  /**
   * Determines if the image is served from a globally allowed host.
   *
   * @param string $src
   *   The image's src.
   *
   * @return bool
   *   Indicates if the image is served from a globally allowed host.
   */
  protected function isAllowedGlobal($src) {
    $allowed = $this->configFactory->get('image_domain_filter.settings')
      ->get('allowed_hosts');
    return $this->isAllowed($src, $allowed);
  }

  /**
   * Determines if the image is served from a host trusted by the site config.
   *
   * @param string $src
   *   The image's src.
   *
   * @return bool
   *   Indicates if the image is served from a host trusted by the site config.
   */
  protected function isAllowedTrusted($src) {
    return $this->isAllowed($src, Settings::get('trusted_host_patterns', []));
  }

  /**
   * Determine if the image is served from an allowed host.
   *
   * @param string $src
   *   The image's src.
   *
   * @return bool
   *   Indicates if the image is served from an allowed host.
   */
  protected function isAllowedHost($src) {
    return $this->isAllowed($src, $this->settings['allowed_hosts']);
  }

  /**
   * Determine if the image is present locally.
   *
   * @param string $src
   *   The image's src.
   *
   * @return string|false
   *   A relative URL, or FALSE if the image is not found locally.
   */
  protected function isAllowedLocal($src) {
    $base_path = base_path();
    $base_path_length = mb_strlen($base_path);
    $local_dir = \Drupal::root() . '/';

    $src = \Drupal::service('file_url_generator')->transformRelative($src);
    if (mb_substr($src, 0, $base_path_length) != $base_path) {
      return FALSE;
    }

    $local_image_path = $local_dir . mb_substr($src, $base_path_length);
    $local_image_path = rawurldecode($local_image_path);
    if (@getimagesize($local_image_path)) {
      // The image has the right path.
      return $src;
    }

    return FALSE;
  }

  /**
   * Determines if the src is allowed.
   *
   * @param string $src
   *   The image's src.
   * @param mixed $allowed
   *   The allowed hosts patterns. This is expected to be an array, but if not
   *   will fail gracefully.
   *
   * @return bool
   *   Indicates if the src is allowed.
   */
  protected function isAllowed($src, $allowed) {
    if (!is_array($allowed)) {
      return FALSE;
    }

    $host = parse_url($src, PHP_URL_HOST);
    if (!$host) {
      return FALSE;
    }

    foreach ($allowed as $item) {
      // Just to match Symfony's request trusted hosts checking.
      $pattern = sprintf('{%s}i', $item);
      if (preg_match($pattern, $host)) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
