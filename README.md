# Image Domain Filter

Drupal core provides a `Restrict images to this site` filter to provide security
for user-uploaded images in text fields. Unfortunately this is incompatible with
sites that host user-loaded files offsite.

This module provides a `Restrict images to trusted sites` filter which allows
administrators to provide a list of allowed hosts/domains for hosting images
used in text fields.

## Installation

Download the module and install into your site's modules directory as you would
any contributed module.

Composer installation:

`composer require drupal/image_domain_filter`

## Configuration

This module is intended to be highly-configurable, and since it leverages the
Drupal configuration system the configuration may be overridden via
`settings.php` or the `config_split` module. Please consider setting these
configurations per environment to allow for the proper security on the uploaded
images.

1. Configure your site's trusted hosts
1. Configure a list of globally-allowed hosts
1. Configure each text format to utilize the "Restrict images to trusted sites"
   filter with the desired hosts/domains.

### Configure your site's trusted hosts

As a Drupal best practice, define your site's `trusted_host_patterns` in your
`settings.php` file. See
https://www.drupal.org/docs/installing-drupal/trusted-host-settings#s-trusted-host-security-setting-in-drupal-8
for details on how to configure this. Utilizing these patterns to identify
allowed hosts will be an option available for each text format.

### Configure globally-allowed hosts

Go to `/admin/config/content/image-domains` and set patterns to identify
hosts/domains which should be allowed for images. These should be set 1 per line
and use the same regex format as the `trusted_host_patterns`. Utilizing these
patterns to identify allowed hosts will be an option available for each text
format.

### Configure text formats

For each text format, you will want to:

1. Remove the `Restrict images to this site` filter
1. Add the `Restrict images to trusted site` filter
1. Configure the `Restrict images to trusted site` filter
   1. Check the `Allow images from this site` checkbox to allow images found on
      the local file system. This is the same behavior as the `Restrict images
      to this site` filter, but will also allow images matching the other
      options.
   1. Check the `Allow images from site's globally trusted hosts` checkbox to
      allow images hosted on domains that match the globally-allowed hosts
      configuration.
   1. Check the `Allow images from trusted hosts` checkbox to allow images
      hosted on domains that match the site's `trusted_host_patterns`
      configuration.
   1. In the `Allowed hosts` textarea, add any other patterns. Images hosted on
      domains that match these values will be allowed. Note, these values apply
      only to the format which the value is configured on.
